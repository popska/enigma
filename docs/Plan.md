# overall purpose

The purpose is to create an enigma machine emulator which is given a config file, and a message file. The message is then encrypted/decrypted.

# functional examples



# data flow

* the letters will be translated to numbers so they will can work as array indices (a -> 0, b -> 1, ... z -> 25)
* the numbers will go keyboard -> plugboard -> rotor1 -> rotor2 -> rotor3 -> reflector -> rotor3 -> rotor2 -> rotor1 -> plugboard -> display
* the reflector will be a kind of rotor (it has reciprocal property & doesn't rotate)
* each rotor has a turnover position (when reached next rotor rotates)
* need to figure out double stepping
* `rotor_config` contains configuration for common rotors (i.e. I - VIII, ETW, other reflectors, etc.)

# implementation

### enigma

* given plugboard, rotors array, including reflector in constructor
* has method `crypt(message)` which takes the message and runs it through the components in the given order
    * message is an array of numbers given from `main`

### component

* has array called `letters`
* can rotate array by shifting the numbers forward by 1 in the array and moving the last to beginning
* has array called `turnover`
    * contains numbers representing where the next rotor will be prompted to rotate
* has method `rev_crypt` which is like crypt but reverse (returns index of number, not number at index)
* represents a component of the enigma machine (i.e. rotor, reflector, plugboard) which does a substitution cipher. The reflector & plugboards are components, while the rotor class inherits from it, but with the ability to rotate.
* has method `crypt` which takes a letter and then passes it through substitution cipher

### enigma\_builder

The given config file looks like:
```
plugboard: AB CD EF GH
rotors: RotorI RotorIV RotorVIII
start: X Y Z
reflector: ReflectorB 
```
Ring setting isn't implemented at this time. Plugboard is all of the pairsof letters which must be swapped.

# build



# testing

