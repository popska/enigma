class Component
  attr_reader :turnover, :letters
  
  def initialize(letters, turnover=[])
    @letters = letters
    @turnover = turnover
  end

  def crypt(index)
    @letters[index]
  end

  def rev_crypt(number)
    @letters.index(number)
  end

  def rotate
    @letters.unshift(@letters.pop())
  end
end
