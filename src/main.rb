require_relative 'enigma_builder'

include EnigmaBuilder

def read_msg(file_name)
  msg = ""
  open(file_name) do |file|
    while line = file.gets
      msg += line.scan(/\w+/).join.upcase
    end
  end
  msg
end

enigma = build(ARGV[0])

puts to_let(enigma.crypt(to_num(read_msg(ARGV[1]))))

