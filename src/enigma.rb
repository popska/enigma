require_relative 'component'

class Enigma
  def initialize(plugboard, rotors, reflector)
    @plugboard = plugboard
    @rotors = rotors
    @reflector = reflector
  end

  def crypt(message)
    crypt_msg = []

    (message).each do |number|
      number = @plugboard.crypt(number)
      
      (0...@rotors.length).each do |i|
        if i == 0 || @rotors[i - 1].turnover.include?(@rotors[i - 1].letters[0])
          @rotors[i].rotate
        end
        number = @rotors[i].crypt(number)
      end

      number = @reflector.crypt(number)

      (0...@rotors.length).reverse_each do |i|
        number = @rotors[i].rev_crypt(number)
      end

      number = @plugboard.crypt(number)

      crypt_msg << number
    end
    crypt_msg
  end
end
