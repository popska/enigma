require_relative 'enigma'
require_relative 'component_config'

include ComponentConfig

module EnigmaBuilder
  def build(file_name)
    pb, rotors, ref = nil  

    open(file_name) do |file|
      while line = file.gets
        if line[/\Aplugboard:/]
          pb = build_plugboard(line.split(" ").drop(1)) 
        elsif line[/\Arotors:/]
          rotors = build_rotors(line.split(" ").drop(1))
        elsif line[/\Areflector:/]
          ref = build_reflector(line.split(" ").drop(1))
        elsif line[/\Astart:/]
          set_start_pos(rotors, line.split(" ").drop(1))
        end
      end
    end
    Enigma.new(pb, rotors, ref)
  end

  # takes string and returns array of integers
  def to_num(letters)
    numbers = []
    (letters).each_char do |letter|
      numbers << letter.ord - 65
    end
    numbers
  end

  # takes an array of integers and returns string
  def to_let(numbers)
    letters = ""
    (numbers).each do |number|
      letters += (number + 65).chr
    end
    letters
  end

  def build_plugboard(pairs)
    pb = ReflectorETW  # set plugboard such that every letter pairs with itself

    (pairs).each do |pair|
      idx0, idx1 = pb.index(pair[0]), pb.index(pair[1])
      pb[idx0], pb[idx1] = pair[1], pair[0]
    end
    Component.new(to_num(pb))
  end

  def build_rotors(rotor_list)
    rotors = []
    (rotor_list).each do |rotor|
      rotor = eval(rotor)  # XXX: replace with if/else block 
      rotors << Component.new(to_num(rotor[0]), to_num(rotor[1]))
    end
    rotors
  end

  def build_reflector(reflector)
    Component.new(to_num(eval(reflector[0])))  # XXX
  end

  def set_start_pos(rotors, start_pos)
    if rotors.length != start_pos.length
      exit
    else
      (0...start_pos.length).each do |i|
        (to_num(start_pos[i])[0]).times do
          rotors[i].rotate
        end
      end
    end
  end
end

